'use strict'
const CommonsChunkPlugin = require('webpack').optimize.CommonsChunkPlugin
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')
const path = require('path')

module.exports = {
    entry: {
        'vendor': './src/vendor.js',
        'solution': './src/solution/solution.module.js',
        'main': './src/main.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                // Disable AMD on every dependency
                include: /node_modules/,
                loader: 'imports-loader?define.amd=>false'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader?+cacheDirectory'
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    interpolate: true,
                }
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader?importLoaders=1',
                })
            },
        ]
    },
    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    },
    plugins: [
        new CommonsChunkPlugin({
            names: [ 'vendor', 'solution' ],
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
        }),
        new ExtractTextPlugin({
            filename: 'styles.css',
            allChunks: true,
        }),
        new CleanPlugin([ 'dist' ]),
    ],
}
